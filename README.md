# tourmaline

Tourmaline is a (planned) agnostic, asynchronous and awesome installer framework


## Usage

Just run `cargo run -- help` or `trm help` for now.


## Distro Config

Scripts and config files for all supported distros are stored in the `config` folder of this repository.
Each distro has its own folder with several subfolders corresponding to all integration tasks.

```
<distro>
  | distro.toml
  | config.schema.json
  | <task>
      | up.nu
      | down.nu 
  | <task>
      | up.nu
      | down.nu
```

### Distro config

The `distro.toml` file contains config options of the distro and allows one to define additional
tasks to perform on install.
The basic schema of this config file looks like this:

```toml
# Distro metadata
[distro]

# Name of the distro
name = "Distro Name"

# Website of the distro
website = "https://mydistro.com"


# Configuration of the OSConfig file
[config]

# Reference to the JSON Schema describing the config extension of that distro
#[default: "config.schema.json"]
schema = "config.schema.json"


# Task-related configuration
[tasks]

# The key correspons to the name of the task on the file system.
# In this case the scripts `custom-task/up.nu` and `custom-task/down.nu` must
# exist in the distro's config folder
[tasks.custom-task]

# The key inside the extended config that is used to pass the config
# value to this task
# If this key is empty no value is passed to the task
# [default: none]
config_key = "enable_flatpak"

# If set to true, the task will be skipped if the value of the task's config 
# (indexed by config_key) is null or false. This setting is ignored when
# no config_key is defined for this task
# [default: false]
skip_on_false = true

# The order in which the task(s) should be executed. Note that
# custom tasks alsways get executed after the base task.
# [default: usize::MAX]
order = 10
```

# Config Schema

The config schema describing the extended OSConfig for that distro can be configured in the
`distro.toml` and defaults to `config.schema.json`. 
This file is a [JSON Schema](https://json-schema.org/) describing the additional fields
required in the config. Make sure to mark required values as such so that the config
get's properly validated.


### Scripts

The `up.nu` scripts contain the required steps to apply the change described by the task.
This can include installing packages, creating configuration files and starting systemd units.

The `down.nu` scripts contain the steps to revert the change applied by a task. These scripts will
later be used to offer functionalities like reinstalling grub or changing installed kernels.

A script can declare a global variable `let run_in_chroot = true` to be executed inside the new root. 

# License

> GPL v3

See LICENSE.
