pub mod error;

pub(crate) mod utils;
use std::path::PathBuf;

use distro::{distro_config::DistroConfig, loader::OSConfigLoader};
use task::task_executor::TaskExecutor;
pub use utils::generate_script_files;
pub mod distro;
pub mod task;
use miette::Result;

/// Creates a new executor with the given os config for the current distro
#[tracing::instrument(level = "trace")]
pub async fn create_executor(os_cfg_path: PathBuf) -> Result<TaskExecutor> {
    let distro_config = DistroConfig::load().await?;
    let os_config = OSConfigLoader::new(os_cfg_path, &distro_config)
        .load()
        .await?;

    Ok(TaskExecutor::new(os_config, distro_config))
}
