use std::{io, path::PathBuf};

use miette::Diagnostic;
use thiserror::Error;

pub type AppResult<T> = std::result::Result<T, AppError>;

#[derive(Error, Debug)]
pub enum AppError {
    #[error("Missing config")]
    MissingConfig,

    #[error("IO Error: {0}")]
    Io(#[from] io::Error),

    #[error("JSON deserialization error {0}")]
    JSON(#[from] serde_json::Error),

    #[error(transparent)]
    Script(#[from] ScriptError),

    #[error(transparent)]
    DistroConfig(#[from] DistroConfigError),

    #[error(transparent)]
    Schema(#[from] SchemaError),

    #[error(transparent)]
    OSConfig(#[from] OSConfigError),

    #[error(transparent)]
    Chroot(#[from] ChrootError),
}

#[derive(Error, Debug, Diagnostic)]
pub enum ScriptError {
    #[error("IO Error when trying to read script file: {0}")]
    #[diagnostic(code(trm::script::io_error))]
    Io(#[from] io::Error),

    #[error("Could not find the script file at {0}")]
    #[diagnostic(code(trm::script::not_found))]
    ScriptNotFound(PathBuf),

    #[error("Could not find the main method in the script file: {0}")]
    #[diagnostic(code(trm::script::no_main))]
    MissingMain(PathBuf),
}

#[derive(Error, Debug)]
pub enum DistroConfigError {
    #[error("IO Error when trying to read distro config: {0}")]
    Io(#[from] io::Error),

    #[error("Encountered invalid Toml when parsing distro config: {0}")]
    InvalidToml(#[from] toml::de::Error),
}

#[derive(Error, Debug, Diagnostic)]
pub enum SchemaError {
    #[diagnostic(code(trm::schema::io))]
    #[error("IO Error when trying to read json-schema file: {0}")]
    Io(#[from] io::Error),

    #[error("Encountered invalid JSON when parsing json-schema: {0}")]
    #[diagnostic(code(trm::schema::invalid_json))]
    InvalidJson(#[from] serde_json::Error),

    #[error("Failed to parse the json-schema: {0}")]
    #[diagnostic(code(trm::schema::invalid_schema))]
    ParseSchema(#[from] valico::json_schema::SchemaError),
}

#[derive(Error, Debug, Diagnostic)]
pub enum OSConfigError {
    #[diagnostic(code(trm::os_config::io))]
    #[error("IO Error when trying to read OSConfig file: {0}")]
    Io(#[from] io::Error),

    #[diagnostic(code(trm::os_config::invalid_json))]
    #[error("Encountered invalid JSON when parsing OSConfig: {0}")]
    InvalidJson(#[from] serde_json::Error),

    #[diagnostic(code(trm::os_config::invalid))]
    #[error("The os config is invalid:\n{0}")]
    Validation(String),

    #[diagnostic(code(trm::os_config::missing_key))]
    #[error("Missing config key {0}")]
    MissingConfigKey(String),
}

#[derive(Error, Debug, Diagnostic)]
pub enum ChrootError {
    #[diagnostic(code(trm::chroot::not_found))]
    #[error("Could not find chroot directory {0}")]
    NotFound(PathBuf),

    #[error("Failed to unshare FS resources with parent: {0}")]
    #[diagnostic(code(trm::chroot::unshare))]
    Unshare(io::Error),

    #[error("Failed to create chroot dir: {0}")]
    #[diagnostic(code(trm::chroot::create))]
    CreateChroot(io::Error),

    #[error("Failed to chroot: {0}")]
    #[diagnostic(code(trm::chroot::enter))]
    Chroot(io::Error),

    #[error("Failed to mount directory {0} in chroot: {1}")]
    #[diagnostic(code(trm::chroot::mount))]
    Mount(PathBuf, io::Error),

    #[error("Failed to create symlink to {0} in chroot: {1}")]
    #[diagnostic(code(trm::chroot::symlink))]
    Link(PathBuf, io::Error),

    #[error("Failed to remove symlink in chroot: {0}")]
    #[diagnostic(code(trm::chroot::unlink))]
    Unlink(io::Error),

    #[error("Failed to copy file {0} to chroot: {1}")]
    #[diagnostic(code(trm::chroot::copy_file))]
    Copy(PathBuf, io::Error),

    #[error("Failed to change process working directory: {0}")]
    #[diagnostic(code(trm::chroot::chdir))]
    ChDir(io::Error),
}
