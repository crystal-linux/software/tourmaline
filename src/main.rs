use args::{Args, Command, CreateEmptyConfigArgs, GenerateScriptsArgs, InstallFromConfigArgs};
use clap::Parser;
use miette::{Context, IntoDiagnostic, Result};
use rusty_value::into_json::{EnumRepr, IntoJson, IntoJsonOptions};
use tokio::fs;
use tourmaline::{distro::OSConfig, generate_script_files};
use tracing::metadata::LevelFilter;
use tracing_subscriber::fmt::format::FmtSpan;

mod args;

#[tokio::main(flavor = "current_thread")]
async fn main() -> miette::Result<()> {
    miette::set_panic_hook();
    color_eyre::install().unwrap();
    let _ = dotenv::dotenv();
    let args = Args::parse();

    if args.verbose {
        init_tracing(LevelFilter::DEBUG);
    } else {
        init_tracing(LevelFilter::INFO);
    }

    match args.command {
        Command::InstallFromConfig(args) => install_from_config(args).await,
        Command::GenerateScripts(args) => generate_scripts(args).await,
        Command::CreateEmptyConfig(args) => generate_empty_config(args).await,
    }?;

    Ok(())
}

/// Installs the distro from a given configuration file
async fn install_from_config(args: InstallFromConfigArgs) -> Result<()> {
    tourmaline::create_executor(args.path)
        .await?
        .with_base_tasks()
        .with_custom_tasks()
        .execute()
        .await
}

async fn generate_scripts(args: GenerateScriptsArgs) -> Result<()> {
    generate_script_files(args.path).await
}

async fn generate_empty_config(args: CreateEmptyConfigArgs) -> Result<()> {
    let config = OSConfig::empty().into_json_with_options(&IntoJsonOptions {
        enum_repr: EnumRepr::Untagged,
    });
    let config_string = serde_json::to_string_pretty(&config)
        .into_diagnostic()
        .context("serializing default config")?;
    fs::write(args.path, config_string)
        .await
        .into_diagnostic()
        .context("writing empty config")?;

    Ok(())
}

fn init_tracing(max_level: LevelFilter) {
    tracing_subscriber::fmt::SubscriberBuilder::default()
        .with_max_level(max_level)
        .with_writer(std::io::stderr)
        .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
        .compact()
        .init();
}
