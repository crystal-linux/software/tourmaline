use std::path::Path;
use std::{env, path::PathBuf};

use tokio::fs;

use crate::task::base_task::ALL_BASE_TASKS;
use miette::{Context, IntoDiagnostic, Result};

const DEFAULT_CONFIG_DIR: &str = "/etc";

macro_rules! env_cfg {
    ($name:ident: $type:ident <- $env_key:literal default $default:expr) => {
        lazy_static::lazy_static! {
            pub static ref $name: $type = env::var($env_key).map($type::from).unwrap_or_else(|_| $default);
        }
    }
}

env_cfg!(CFG_PATH: PathBuf <- "TRM_CFG_PATH" default PathBuf::from(DEFAULT_CONFIG_DIR).join("tourmaline"));
env_cfg!(ROOT_MNT: PathBuf <- "TRM_ROOT_MNT" default PathBuf::from("/mnt"));

pub async fn generate_script_files<P: AsRef<Path>>(output: P) -> Result<()> {
    let output = output.as_ref();

    for task in &*ALL_BASE_TASKS {
        let key_data = task.key_data();
        let name = key_data.task_name;
        let script_dir = output.join(name);

        if !script_dir.exists() {
            fs::create_dir_all(&script_dir)
                .await
                .into_diagnostic()
                .context("creating script dir")?;
        }
        let up_path = output.join("up.nu");
        let down_path = output.join("down.nu");

        fs::write(
            &up_path,
            format!(
                r#"# Applies all system changes of `{name}`
def main [cfg] {{
    echo "Executing up task `{name}` with config" $cfg
}}
"#
            ),
        )
        .await
        .into_diagnostic()
        .context("writing up task content")?;

        fs::write(
            &down_path,
            format!(
                r#"# Reverts all system changes of `{name}`
def main [cfg] {{
    echo "Executing up task `{name}` with config" $cfg
}}
"#
            )
            .trim(),
        )
        .await
        .into_diagnostic()
        .context("writing down task content")?;
    }

    Ok(())
}
