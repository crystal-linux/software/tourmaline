use std::path::PathBuf;

use embed_nu::IntoValue;

use crate::distro::OSConfig;

use super::{exec_builder::ExecBuilder, TaskTrait};
use miette::Result;

#[derive(Clone, Debug)]
pub struct CustomTask {
    config_key: Option<String>,
    up_script: PathBuf,
    down_script: PathBuf,
    skip_on_false: bool,
    order: usize,
}

impl CustomTask {
    pub fn new(
        name: String,
        config_key: Option<String>,
        skip_on_false: bool,
        order: usize,
    ) -> Self {
        let base_path = PathBuf::from(name);
        Self {
            config_key,
            up_script: base_path.join("up.nu"),
            down_script: base_path.join("down.nu"),
            skip_on_false,
            order,
        }
    }
}

impl TaskTrait for CustomTask {
    #[tracing::instrument(level = "trace", skip_all)]
    fn up(&self, config: &OSConfig) -> Result<Option<ExecBuilder>> {
        let task_config = if let Some(key) = self.config_key.as_ref() {
            config.get_nu_value(key)?
        } else {
            Option::<()>::None.into_value()
        };

        if self.skip_on_false && self.config_key.is_some() && config_is_falsy(&task_config) {
            Ok(None)
        } else {
            let exec =
                ExecBuilder::create(self.up_script.to_owned(), config.to_owned(), task_config)?;
            Ok(Some(exec))
        }
    }

    #[tracing::instrument(level = "trace", skip_all)]
    fn down(&self, config: &OSConfig) -> Result<Option<ExecBuilder>> {
        let task_config = if let Some(key) = self.config_key.as_ref() {
            config.get_nu_value(key)?
        } else {
            Option::<()>::None.into_value()
        };

        if self.skip_on_false && self.config_key.is_some() && config_is_falsy(&task_config) {
            Ok(None)
        } else {
            let exec =
                ExecBuilder::create(self.down_script.to_owned(), config.to_owned(), task_config)?;
            Ok(Some(exec))
        }
    }

    fn order(&self) -> usize {
        self.order
    }
}

fn config_is_falsy(config: &embed_nu::Value) -> bool {
    if config.is_nothing() {
        return true;
    } else if let Ok(b) = config.as_bool() {
        return !b;
    }
    false
}
