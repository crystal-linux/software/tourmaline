use std::{
    ffi::{c_int, CString, OsString},
    io,
    os::unix::prelude::OsStrExt,
    path::{Path, PathBuf},
};

use libc::CLONE_FS;

use tokio::task::JoinHandle;
mod mapping;
pub mod setup;
pub mod teardown;

use crate::error::ChrootError;

use self::mapping::MappingHandle;

pub struct Chroot {
    root_path: PathBuf,
    _mappings: Vec<MappingHandle>,
}

impl Chroot {
    /// Runs the given future in a new chroot
    pub fn run<F, T>(&self, call: F) -> JoinHandle<Result<T, ChrootError>>
    where
        F: FnOnce() -> T + Send + 'static,
        T: Send + 'static,
    {
        let root_path = self.root_path.clone();
        let handle = std::thread::spawn(move || {
            unsafe {
                init_chroot(&root_path)?;
            }
            Ok(call())
        });
        tokio::task::spawn_blocking(|| handle.join().unwrap())
    }
}

unsafe fn init_chroot(path: &Path) -> Result<(), ChrootError> {
    handle_err_code(libc::unshare(CLONE_FS)).map_err(ChrootError::Unshare)?;
    let path_str = c_str(path);
    handle_err_code(libc::chroot(
        path_str.as_bytes_with_nul().as_ptr() as *const libc::c_char
    ))
    .map_err(ChrootError::Chroot)?;
    std::env::set_current_dir("/").map_err(ChrootError::ChDir)?;
    std::env::set_var("PWD", "/");

    Ok(())
}

fn handle_err_code(code: c_int) -> Result<(), io::Error> {
    if code != 0 {
        Err(io::Error::last_os_error())
    } else {
        Ok(())
    }
}

pub(crate) fn c_str<O: Into<OsString>>(o: O) -> CString {
    CString::new(o.into().as_bytes().to_vec()).unwrap()
}
