use std::path::PathBuf;

use tokio::fs;

use crate::error::ChrootError;

use super::{mapping::default_mappings, Chroot};
use miette::Result;

impl Chroot {
    /// Creates a new chroot with the given path
    pub async fn create<P: Into<PathBuf>>(root_path: P) -> Result<Self> {
        let root_path = root_path.into();
        if !root_path.exists() {
            fs::create_dir_all(&root_path)
                .await
                .map_err(ChrootError::CreateChroot)?;
        }
        let default_mappings = default_mappings();
        let mut handles = Vec::with_capacity(default_mappings.len());

        for mapping in default_mappings {
            let handle = mapping.create_mapping(&root_path).await?;
            handles.push(handle);
        }

        Ok(Self {
            root_path,
            _mappings: handles,
        })
    }
}
