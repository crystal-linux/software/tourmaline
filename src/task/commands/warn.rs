use embed_nu::{
    nu_protocol::{engine::Command, Signature, SyntaxShape},
    CallExt, PipelineData,
};

#[derive(Clone)]
pub struct WarnCommand;

impl Command for WarnCommand {
    fn name(&self) -> &str {
        "warn"
    }

    fn signature(&self) -> embed_nu::nu_protocol::Signature {
        Signature::new("warn")
            .rest("rest", SyntaxShape::Any, "the warning to print")
            .category(embed_nu::nu_protocol::Category::Custom("Tourmaline".into()))
    }

    fn usage(&self) -> &str {
        "Prints the given message and values with warning severity"
    }

    fn run(
        &self,
        engine_state: &embed_nu::nu_protocol::engine::EngineState,
        stack: &mut embed_nu::nu_protocol::engine::Stack,
        call: &embed_nu::nu_protocol::ast::Call,
        _input: embed_nu::PipelineData,
    ) -> Result<embed_nu::PipelineData, embed_nu::nu_protocol::ShellError> {
        let args: Vec<String> = call.rest(engine_state, stack, 0)?;
        tracing::warn!("{}", args.join(" "));

        Ok(PipelineData::empty())
    }
}
