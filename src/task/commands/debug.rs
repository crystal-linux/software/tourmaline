use embed_nu::{
    nu_protocol::{engine::Command, Signature, SyntaxShape},
    CallExt, PipelineData,
};

#[derive(Clone)]
pub struct DebugCommand;

impl Command for DebugCommand {
    fn name(&self) -> &str {
        "debug"
    }

    fn signature(&self) -> embed_nu::nu_protocol::Signature {
        Signature::new("debug")
            .rest("rest", SyntaxShape::Any, "the message to print")
            .category(embed_nu::nu_protocol::Category::Custom("Tourmaline".into()))
    }

    fn usage(&self) -> &str {
        "Prints the given message and values with debug severity (only when --verbose is passed to tourmaline)"
    }

    fn run(
        &self,
        engine_state: &embed_nu::nu_protocol::engine::EngineState,
        stack: &mut embed_nu::nu_protocol::engine::Stack,
        call: &embed_nu::nu_protocol::ast::Call,
        _input: embed_nu::PipelineData,
    ) -> Result<embed_nu::PipelineData, embed_nu::nu_protocol::ShellError> {
        let args: Vec<String> = call.rest(engine_state, stack, 0)?;
        tracing::debug!("{}", args.join(" "));

        Ok(PipelineData::empty())
    }
}
