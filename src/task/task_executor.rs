use crate::{
    distro::{distro_config::DistroConfig, OSConfig},
    utils::ROOT_MNT,
};

use super::{
    base_task::ALL_BASE_TASKS, chrooting::Chroot, custom_task::CustomTask, Task, TaskTrait,
};

pub struct TaskExecutor {
    distro_config: DistroConfig,
    os_config: OSConfig,
    tasks: Vec<Task>,
}
use miette::Result;

impl TaskExecutor {
    /// Creates a new task executor with the given OSConfig and Distro Config
    pub fn new(os_config: OSConfig, distro_config: DistroConfig) -> Self {
        Self {
            distro_config,
            os_config,
            tasks: Vec::new(),
        }
    }

    /// Adds all base tasks to the executor
    #[tracing::instrument(level = "trace", skip_all)]
    pub fn with_base_tasks(&mut self) -> &mut Self {
        let mut base_tasks = (*ALL_BASE_TASKS)
            .iter()
            .cloned()
            .map(Task::Base)
            .collect::<Vec<_>>();
        self.tasks.append(&mut base_tasks);

        self
    }

    /// Adds all custom tasks to the executor
    #[tracing::instrument(level = "trace", skip_all)]
    pub fn with_custom_tasks(&mut self) -> &mut Self {
        let mut custom_tasks = self
            .distro_config
            .tasks
            .iter()
            .map(|(name, task)| {
                CustomTask::new(
                    name.to_owned(),
                    task.config_key.to_owned(),
                    task.skip_on_false,
                    task.order,
                )
            })
            .map(Task::Custom)
            .collect::<Vec<_>>();
        self.tasks.append(&mut custom_tasks);

        self
    }

    /// Executes all tasks
    #[tracing::instrument(level = "trace", skip_all)]
    pub async fn execute(&mut self) -> Result<()> {
        self.tasks.sort_by(Task::compare);
        let mut chroot = None;

        for task in &self.tasks {
            if let Some(up_task) = task.up(&self.os_config)? {
                if up_task.requires_chroot() {
                    if chroot.is_none() {
                        chroot = Some(Chroot::create(&*ROOT_MNT).await?);
                    }
                    chroot
                        .as_ref()
                        .unwrap()
                        .run(|| up_task.exec())
                        .await
                        .unwrap()??;
                } else {
                    up_task.exec()?;
                }
            }
        }

        Ok(())
    }
}
