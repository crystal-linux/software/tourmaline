use std::path::PathBuf;

use crate::distro::OSConfig;

use super::{exec_builder::ExecBuilder, TaskTrait};
use embed_nu::IntoValue;
use lazy_static::lazy_static;
use miette::Result;

#[derive(Clone, Debug)]
pub enum BaseTask {
    ConfigureLocale,
    ConfigureNetwork,
    CreatePartitions,
    InstallBase,
    InstallBootloader,
    InstallDesktop,
    InstallKernels,
    InstallExtraPackages,
    SetupRootUser,
    SetupUsers,
}

#[derive(Clone, Copy)]
pub struct BaseTaskKeydata {
    pub task_name: &'static str,
    pub config_key: Option<&'static str>,
}

impl BaseTask {
    pub fn key_data(&self) -> BaseTaskKeydata {
        match self {
            BaseTask::ConfigureLocale => BaseTaskKeydata {
                task_name: "configure-locale",
                config_key: Some("locale"),
            },
            BaseTask::ConfigureNetwork => BaseTaskKeydata {
                task_name: "configure-network",
                config_key: Some("network"),
            },
            BaseTask::CreatePartitions => BaseTaskKeydata {
                task_name: "create-partitions",
                config_key: Some("partitions"),
            },
            BaseTask::InstallBase => BaseTaskKeydata {
                task_name: "install-base",
                config_key: None,
            },
            BaseTask::InstallBootloader => BaseTaskKeydata {
                task_name: "install-bootloader",
                config_key: Some("bootloader"),
            },
            BaseTask::InstallDesktop => BaseTaskKeydata {
                task_name: "install-desktop",
                config_key: Some("desktop"),
            },
            BaseTask::InstallExtraPackages => BaseTaskKeydata {
                task_name: "install-extra-packages",
                config_key: Some("extra_packages"),
            },
            BaseTask::SetupRootUser => BaseTaskKeydata {
                task_name: "setup-root-user",
                config_key: Some("root_user"),
            },
            BaseTask::SetupUsers => BaseTaskKeydata {
                task_name: "setup-users",
                config_key: Some("users"),
            },
            BaseTask::InstallKernels => BaseTaskKeydata {
                task_name: "install-kernels",
                config_key: Some("kernels"),
            },
        }
    }
}

impl TaskTrait for BaseTask {
    #[tracing::instrument(level = "trace", skip_all)]
    fn up(&self, config: &OSConfig) -> Result<Option<ExecBuilder>> {
        let key_data = self.key_data();
        let script = PathBuf::from(key_data.task_name).join("up.nu");

        let task_config = if let Some(key) = key_data.config_key {
            config.get_nu_value(key)?
        } else {
            Option::<()>::None.into_value()
        };
        let exec = ExecBuilder::create(script, config.to_owned(), task_config)?;

        Ok(Some(exec))
    }

    #[tracing::instrument(level = "trace", skip_all)]
    fn down(&self, config: &OSConfig) -> Result<Option<ExecBuilder>> {
        let key_data = self.key_data();
        let script = PathBuf::from(key_data.task_name).join("down.nu");
        let task_config = if let Some(key) = key_data.config_key {
            config.get_nu_value(key)?
        } else {
            Option::<()>::None.into_value()
        };
        let exec = ExecBuilder::create(script, config.to_owned(), task_config)?;

        Ok(Some(exec))
    }

    fn order(&self) -> usize {
        match self {
            BaseTask::CreatePartitions => 10,
            BaseTask::InstallBase => 20,
            BaseTask::InstallKernels => 30,
            BaseTask::InstallBootloader => 40,
            BaseTask::ConfigureLocale => 50,
            BaseTask::ConfigureNetwork => 60,
            BaseTask::SetupRootUser => 70,
            BaseTask::SetupUsers => 80,
            BaseTask::InstallDesktop => 90,
            BaseTask::InstallExtraPackages => 100,
        }
    }
}

lazy_static! {
    pub static ref ALL_BASE_TASKS: Vec<BaseTask> = get_all_base_tasks();
}

fn get_all_base_tasks() -> Vec<BaseTask> {
    vec![
        BaseTask::ConfigureLocale,
        BaseTask::ConfigureNetwork,
        BaseTask::CreatePartitions,
        BaseTask::InstallBase,
        BaseTask::InstallBootloader,
        BaseTask::InstallDesktop,
        BaseTask::InstallExtraPackages,
        BaseTask::SetupRootUser,
        BaseTask::SetupUsers,
        BaseTask::InstallKernels,
    ]
}
