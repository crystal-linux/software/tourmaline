use std::cmp::Ordering;

use crate::distro::OSConfig;

use self::{base_task::BaseTask, custom_task::CustomTask, exec_builder::ExecBuilder};
pub mod base_task;
mod chrooting;
mod commands;
pub mod custom_task;
pub mod exec_builder;
pub mod task_executor;
use miette::Result;

pub trait TaskTrait {
    fn up(&self, config: &OSConfig) -> Result<Option<ExecBuilder>>;
    fn down(&self, config: &OSConfig) -> Result<Option<ExecBuilder>>;
    /// Used to decide the execution order
    /// smaller values mean the task get's executed earlier
    fn order(&self) -> usize;
}

#[derive(Clone, Debug)]
pub enum Task {
    Base(BaseTask),
    Custom(CustomTask),
}

impl Task {
    pub fn is_custom(&self) -> bool {
        match self {
            Task::Base(_) => false,
            Task::Custom(_) => true,
        }
    }

    pub fn is_base(&self) -> bool {
        !self.is_custom()
    }

    pub fn compare(&self, other: &Self) -> Ordering {
        if self.is_base() && other.is_custom() {
            Ordering::Less
        } else if self.is_custom() && other.is_base() || self.order() > other.order() {
            Ordering::Greater
        } else if self.order() < other.order() {
            Ordering::Less
        } else {
            Ordering::Equal
        }
    }
}

impl TaskTrait for Task {
    #[inline]
    fn up(&self, config: &OSConfig) -> Result<Option<ExecBuilder>> {
        match self {
            Task::Base(b) => b.up(config),
            Task::Custom(c) => c.up(config),
        }
    }

    #[inline]
    fn down(&self, config: &OSConfig) -> Result<Option<ExecBuilder>> {
        match self {
            Task::Base(b) => b.down(config),
            Task::Custom(c) => c.down(config),
        }
    }

    fn order(&self) -> usize {
        match self {
            Task::Base(b) => b.order(),
            Task::Custom(c) => c.order(),
        }
    }
}
