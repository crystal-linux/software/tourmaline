use std::{collections::HashMap, path::PathBuf};

use serde::Deserialize;
use tokio::fs;

use crate::utils::CFG_PATH;
use miette::{self, Context, IntoDiagnostic, Result};

/// The config file of a distro that defines
/// how that distro should be installed
#[derive(Clone, Debug, Deserialize)]
pub struct DistroConfig {
    /// Metadata about the distro
    pub distro: DistroMetadata,

    /// Configuration related to the Operating system
    /// setup configuration
    pub config: OSConfigMetadata,

    /// Additional distro specific tasks
    pub tasks: HashMap<String, TaskConfig>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct DistroMetadata {
    /// The name of the distro
    pub name: String,

    /// The website of the distro
    pub website: Option<String>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct OSConfigMetadata {
    /// The path of the config schema file
    pub schema: Option<PathBuf>,
}

/// The configuration of a single task
#[derive(Clone, Debug, Deserialize)]
pub struct TaskConfig {
    /// The name of the config field
    pub config_key: Option<String>,

    /// If the task should be skipped if the
    /// config value of that task is null
    #[serde(default)]
    pub skip_on_false: bool,

    /// The execution order of this task
    /// Note that custom tasks always get executed after
    /// the base tasks
    /// If not set this value defaults to [usize::MAX]
    #[serde(default = "default_order")]
    pub order: usize,
}

#[inline]
fn default_order() -> usize {
    usize::MAX
}

impl DistroConfig {
    #[tracing::instrument(level = "trace", skip_all)]
    pub async fn load() -> Result<Self> {
        let path = CFG_PATH.join("distro.toml");
        let contents = fs::read_to_string(path)
            .await
            .into_diagnostic()
            .context("reading config file")?;
        let cfg = toml::from_str::<Self>(&contents).into_diagnostic()?;

        Ok(cfg)
    }
}
