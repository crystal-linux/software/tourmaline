use std::path::PathBuf;

use tokio::fs;
use valico::json_schema::Scope;

use crate::{
    distro::distro_config::DistroConfig,
    error::{OSConfigError, SchemaError},
    utils::CFG_PATH,
};
use miette::{IntoDiagnostic, Result};

use super::OSConfig;

#[derive(Debug)]
pub struct OSConfigLoader<'a> {
    distro_cfg: &'a DistroConfig,
    cfg_path: PathBuf,
}

impl<'a> OSConfigLoader<'a> {
    pub fn new(path: PathBuf, distro_cfg: &'a DistroConfig) -> Self {
        Self {
            distro_cfg,
            cfg_path: path,
        }
    }

    #[tracing::instrument(level = "trace", skip_all)]
    pub async fn load(&self) -> Result<OSConfig> {
        let schema = self.load_extension_schema().await?;
        let os_config = OSConfig::load(&self.cfg_path).await?;
        Self::validate_config(schema, &os_config)?;

        Ok(os_config)
    }

    #[tracing::instrument(level = "trace", skip_all)]
    async fn load_extension_schema(&self) -> Result<serde_json::Value> {
        let schema_path = self
            .distro_cfg
            .config
            .schema
            .as_ref()
            .map(|p| CFG_PATH.join(p))
            .unwrap_or_else(|| CFG_PATH.join("config.schema.json"));
        let contents = fs::read_to_string(schema_path).await.into_diagnostic()?;
        let schema = serde_json::from_str(&contents).into_diagnostic()?;

        Ok(schema)
    }

    #[tracing::instrument(level = "trace", skip_all)]
    fn validate_config(schema: serde_json::Value, config: &OSConfig) -> Result<()> {
        let mut scope = Scope::new();
        let schema = scope
            .compile_and_return(schema, true)
            .map_err(SchemaError::ParseSchema)?;
        let ext_value = serde_json::Value::Object(config.extended.clone().into_iter().collect());

        let result = schema.validate(&ext_value);

        if result.is_valid() {
            tracing::debug!("Config is valid");
            Ok(())
        } else {
            let msg = result
                .errors
                .into_iter()
                .map(|e| format!("{} > {}", e.get_path(), e.get_title()))
                .collect::<Vec<_>>()
                .join("\n");
            tracing::error!("Config is invalid");

            Err(OSConfigError::Validation(msg).into())
        }
    }
}
