use std::path::PathBuf;

use embed_nu::rusty_value::*;
use serde::Deserialize;

/// The base configuration of the operating system
/// This config alone should provide all required configuraiton
/// values to create a base distro installation while still being
/// distro agnostic.
#[derive(Clone, Debug, Deserialize, RustyValue)]
pub struct BaseConfig {
    pub locale: LocaleConfig,
    pub network: NetworkConfig,
    pub partitions: PartitionsConfig,
    pub bootloader: BootloaderConfig,
    pub kernels: KernelConfig,
    pub desktop: DesktopConfig,
    pub users: UsersConfig,
    pub root_user: RootUserConfig,
    pub extra_packages: ExtraPackages,
}

impl BaseConfig {
    pub(crate) fn empty() -> Self {
        Self {
            locale: LocaleConfig {
                locale: Vec::new(),
                keymap: String::new(),
                timezone: String::new(),
            },
            network: NetworkConfig {
                hostname: String::new(),
                ipv6_loopback: false,
            },
            partitions: PartitionsConfig {
                device: PathBuf::new(),
                efi_partition: false,
                partitions: Partitions::Auto,
            },
            bootloader: BootloaderConfig {
                preset: BootloaderPreset::GrubEfi,
                location: PathBuf::new(),
            },
            kernels: KernelConfig {
                default: Kernel(String::new()),
                additional: Vec::new(),
            },
            desktop: DesktopConfig::KdePlasma,
            users: UsersConfig(Vec::new()),
            root_user: RootUserConfig {
                password: String::new(),
            },
            extra_packages: Vec::new(),
        }
    }
}

#[derive(Clone, Deserialize, RustyValue, Debug)]
pub struct LocaleConfig {
    pub locale: Vec<String>,
    pub keymap: String,
    pub timezone: String,
}

#[derive(Clone, Debug, Deserialize, RustyValue)]
pub struct NetworkConfig {
    pub hostname: String,
    pub ipv6_loopback: bool,
}

#[derive(Clone, Debug, Deserialize, RustyValue)]
pub struct PartitionsConfig {
    pub device: PathBuf,
    pub efi_partition: bool,
    pub partitions: Partitions,
}

#[derive(Clone, Debug, Deserialize, RustyValue)]
pub enum Partitions {
    Auto,
    Manual(Vec<Partition>),
}

#[derive(Clone, Debug, Deserialize, RustyValue)]
pub struct Partition {
    pub mountpoint: PathBuf,
    pub blockdevice: PathBuf,
    pub filesystem: Option<FileSystem>,
}

#[derive(Clone, Debug, Deserialize, RustyValue)]
pub enum FileSystem {
    VFat,
    Bfs,
    CramFS,
    Ext2,
    Ext3,
    Ext4,
    Fat,
    Msdos,
    Xfs,
    Btrfs,
    Minix,
    F2fs,
}

#[derive(Clone, Debug, Deserialize, RustyValue)]
pub struct BootloaderConfig {
    pub preset: BootloaderPreset,
    pub location: PathBuf,
}

#[derive(Clone, Debug, Deserialize, RustyValue)]
pub enum BootloaderPreset {
    GrubEfi,
    Legacy,
}

#[derive(Clone, Debug, Deserialize, RustyValue)]
pub enum DesktopConfig {
    Onyx,
    KdePlasma,
    Mate,
    Gnome,
    Cinnamon,
    Xfce,
    Budgie,
    Enlightenment,
    Lxqt,
    Sway,
    I3Gaps,
    HerbstluftWM,
    AwesomeWM,
    Bspwm,
}

pub type ExtraPackages = Vec<String>;

#[derive(Clone, Debug, RustyValue, Deserialize)]
pub struct KernelConfig {
    pub default: Kernel,
    pub additional: Vec<Kernel>,
}

#[derive(Clone, Debug, RustyValue, Deserialize)]
pub struct Kernel(pub String);

#[derive(Clone, Debug, Deserialize, RustyValue)]
pub struct RootUserConfig {
    pub password: String,
}

#[derive(Clone, Debug, Deserialize, RustyValue)]
pub struct UsersConfig(Vec<User>);

#[derive(Clone, Debug, Deserialize, RustyValue)]
pub struct User {
    pub name: String,
    pub password: String,
    pub sudoer: bool,
    pub shell: String,
}
