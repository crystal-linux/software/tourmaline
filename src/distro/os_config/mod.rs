mod base_config;
use std::{collections::HashMap, path::Path};

pub use base_config::BaseConfig;
pub mod loader;
use embed_nu::{
    rusty_value::{
        Fields, Float, HashablePrimitive, HashableValue, Integer, Primitive, RustyValue, Struct,
        Value,
    },
    RustyIntoValue,
};
use miette::{Context, IntoDiagnostic, Result};
use serde::Deserialize;
use tokio::fs;

use crate::error::OSConfigError;

/// Represents the full configuration of the OS including extensions defined
/// by the distro
#[derive(Clone, Debug, Deserialize)]
pub struct OSConfig {
    #[serde(flatten)]
    pub base: BaseConfig,
    #[serde(flatten)]
    pub extended: HashMap<String, serde_json::Value>,
}

impl OSConfig {
    pub fn get_nu_value<K: AsRef<str>>(&self, key: K) -> Result<embed_nu::Value> {
        let value = self.clone().into_rusty_value();
        let mut fields = if let Value::Struct(Struct { fields, .. }) = value {
            if let Fields::Named(named) = fields {
                named
            } else {
                panic!("OSConfig fields don't have a name?!");
            }
        } else {
            panic!("OSConfig is not a struct?!");
        };
        fields
            .remove(key.as_ref())
            .map(|v| v.into_value())
            .ok_or_else(|| OSConfigError::MissingConfigKey(key.as_ref().to_owned()).into())
    }
}

impl RustyValue for OSConfig {
    fn into_rusty_value(self) -> embed_nu::rusty_value::Value {
        let base = self.base.into_rusty_value();

        let mut fields = if let Value::Struct(Struct { fields, .. }) = base {
            if let Fields::Named(named) = fields {
                named
            } else {
                panic!("Base fields don't have a name?!");
            }
        } else {
            panic!("Base is not a struct?!");
        };
        let ext = self.extended.into_iter().map(ext_field_to_rusty_value);
        fields.extend(ext);

        Value::Struct(Struct {
            name: String::from("Config"),
            fields: Fields::Named(fields),
        })
    }
}

impl OSConfig {
    pub fn empty() -> Self {
        Self {
            base: BaseConfig::empty(),
            extended: HashMap::new(),
        }
    }
}

#[inline]
fn ext_field_to_rusty_value(
    entry: (String, serde_json::Value),
) -> (String, embed_nu::rusty_value::Value) {
    (entry.0, json_to_rusty_value(entry.1))
}

fn json_to_rusty_value(val: serde_json::Value) -> embed_nu::rusty_value::Value {
    match val {
        serde_json::Value::Null => Value::None,
        serde_json::Value::Bool(b) => Value::Primitive(Primitive::Bool(b)),
        serde_json::Value::Number(num) => Value::Primitive(if num.is_i64() {
            Primitive::Integer(Integer::I64(num.as_i64().unwrap()))
        } else if num.is_u64() {
            Primitive::Integer(Integer::U64(num.as_u64().unwrap()))
        } else {
            Primitive::Float(Float::F64(num.as_f64().unwrap()))
        }),
        serde_json::Value::String(s) => Value::Primitive(Primitive::String(s)),
        serde_json::Value::Array(a) => {
            Value::List(a.into_iter().map(json_to_rusty_value).collect())
        }
        serde_json::Value::Object(o) => {
            let vals = o
                .into_iter()
                .map(|(k, v)| {
                    (
                        HashableValue::Primitive(HashablePrimitive::String(k)),
                        json_to_rusty_value(v),
                    )
                })
                .collect::<HashMap<_, _>>();
            Value::Map(vals)
        }
    }
}

impl OSConfig {
    pub(crate) async fn load(path: &Path) -> Result<Self> {
        let contents = fs::read_to_string(path)
            .await
            .into_diagnostic()
            .context("reading config contents")?;
        let cfg = serde_json::from_str::<Self>(&contents)
            .into_diagnostic()
            .context("parsing_json")?;

        Ok(cfg)
    }
}
