module utils {
    export def is_ssd [device: string] {
        $device =~ '^/dev/(nvme|mmcblk)'
    }
}

module auto_partition {
    use utils
    
    export def efi [device: string] {
        info "Creating efi partitions"
        efi_layout $device

        if utils is_ssd $device {
            debug "Creating file systems for ssd"
            efi_create_fs_ssd $device
        } else {
            debug "Creating file systems for hdd"
            efi_create_fs_hdd $device
        }
    }

    def efi_layout [device: string] {
        run parted -s $device mklabel gpt
        debug "Partition table created"

        run parted -s $device mkpart fat32 0 300
        debug "EFI partition created"

        run parted -s $device mkpart primary btrfs 512MIB 100%
        debug "Root partition created"
    }

    def efi_create_fs_ssd [device: string] {
    }

    def efi_create_fs_hdd [device: string] {
        let boot_part = $"($device)1";
        let root_part = $"($device)2"

        run mkfs.vfat -F32 $boot_part
        run mkfs.btrfs -f $root_part
        run mount $root_part /mnt
        with-cwd /mnt {
            run btrfs subvolume create @
            run btrfs subvolume create @home
        }
        run umount $root_part
        run mount $root_part /mnt -o subvol=@
        mkdir /mnt/boot/efi
        mkdir /mnt/home
        run mount $root_part /mnt/home -o subvol=@home
        run mount $boot_part /mnt/boot/efi
    }

    export def bios [device: string] {
        debug "Creating bios partitions"
        bios_layout $device
    }

    def bios_layout [device: string] {
        parted -s $device mklabel msdos
        parted -s $device mkpart primary ext4 1MIB 512MIB
        parted -s $device mkpart primary btrfs 512MIB 100%
    }
}

# Applies all system changes of `create-partitions`
def main [cfg] {
    debug $"Creating partitions with config ($cfg)"

    if $cfg.partitions == "Auto" {
        info "Creating partitions automatically..."
        use auto_partition

        if $cfg.efi_partition {
            auto_partition efi $cfg.device 
        } else {
            auto_partition bios $cfg.device 
        }
    } else {
        info "Creating partitions manually"    
    }

    info "Partitions created!"
}
