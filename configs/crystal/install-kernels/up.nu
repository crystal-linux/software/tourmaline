let SUPPORTED_KERNELS = ["linux", "linux-zen", "linus-hardened", "linux-lts"];

# Applies all system changes of `install-kernels`
def main [cfg] {
    debug $"installing kernels with config ($cfg)"
    mut kernel = $cfg.default

    if $kernel not-in $SUPPORTED_KERNELS {
        warn $"Unsupported kernel ($kernel). Defaulting to 'linux' kernel"
        $kernel = "linux"
    }

    run pacstrap /mnt $kernel

    debug "Installing additional kernels"

    $cfg.additional | each {|$k| 
        debug $"installing ($k)"

        if $k in $SUPPORTED_KERNELS {        
            run pacstrap /mnt $k 
        } else {
            warn $"Unsupported kernel ($k)"    
        }
    } 
    info "Kernels installed!"
}
