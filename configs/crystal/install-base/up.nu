def install_base_packages [] {
    (
        run pacstrap /mnt
        base
        linux-firmware    
        systemd-sysvcompat
        networkmanager
        man-db
        man-pages
        texinfo
        nano
        sudo
        curl
        archlinux-keyring

        # crystal base
        crystal-core
        crystal-branding

        # crystal extras
        crystal-first-setup
        neofetch
        btrfs-progs
        which
        base-devel

        # chaotic aur
        chaotic-keyring
        chaotic-mirrorlist

        # fonts
        noto-fonts
        noto-fonts-emoji
        noto-fonts-cjk
        noto-fonts-extra
        ttf-nerd-fonts-symbols-common
        ttf-firacode-nerd
        ttf-liberation

        # audio
        pipewire
        pipewire-pulse
        pipewire-alsa
        pipewire-jack
        wireplumber
        helvum

        # utils
        xterm
        cups
        cups-pdf
        bluez
        bluez-cups
        ntfs-3g
        bash-completion
        zsh-completions
    )
}

# Applies all system changes of `install-base`
def main [cfg] {
    debug $"installing base with config ($cfg)" 

    mkdir /mnt/etc
    install_base_packages
    cp /etc/pacman.conf /mnt/etc/pacman.conf
    run bash -c 'genfstab -U /mnt >> /mnt/etc/fstab'
    info "Base packages installed!"
}
