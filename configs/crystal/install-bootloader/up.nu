let RUN_IN_CHROOT = true;

def install_grub_pkgs [] {
    (run pacman -S --noconfirm
        grub
        efibootmgr
        crystal-grub-theme
        os-prober
        crystal-branding
    )
}

def install_grub_efi [location: string] {
    debug $"Installing grub to ($location)"
    install_grub_pkgs

    (run grub-install
        --target=x86_64-efi
        --efi-directory $location
        --bootloader-id=crystal
        --removable
    ) 
    (run grub-install
        --target=x86_64-efi
        --efi-directory $location
        --bootloader-id=crystal
    )
    "\nGRUB_THEME=\"/usr/share/grub/themes/crystal/theme.txt\"" | save --append /etc/default/grub
    run grub-mkconfig -o /boot/grub/grub.cfg
}

# Applies all system changes of `install-bootloader`
def main [cfg] {
    debug "Installing bootloader with config ($cfg)"

    if $cfg.preset == "GrubEfi" {
        install_grub_efi $cfg.location
    } else {
        error make {msg:  "Not implemented"}
    }
}
